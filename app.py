import os
import random
import json
from flask import Flask, g, jsonify, request, render_template, make_response, flash
from flask_cors import CORS, cross_origin
from werkzeug.utils import secure_filename
import updatemodels
import preprocessdata as ped
from subapi import get_similar
import time
from datetime import date
today = date.today()
today = str(today)
param = [24, 2, 128, 128,1]



#parameters should be {'vector_size':24,'min_count':2,'epochs': 128, 'seed':128, 'workers' 1}




app = Flask(__name__)
app.config.from_object('config')
cors = CORS(app)

PATH_TO_DATASET = app.config['PATH_TO_DATASET']

# Current file directory
here = os.path.dirname(__file__)




# Endpoints
@app.route("/")
def index():
    return ("Document similarity api")
    #return ("welcome to document simarity model")



# Facts alt model with single text and cleaning process
@app.route('/similarities/facts', methods=['POST'])
def modelwithfact():
    facts = request.form['content']
    clean_facts = ped.clean_text(facts)
    # Change to json format
    clean_facts = {"content": clean_facts}
    # Select model that applies
    model = os.path.join(here, 'd2v_model_pickle_2.pickle')
    result = get_similar(content=clean_facts, full_text=text, model_file=model, PATH_TO_DATASET=PATH_TO_DATASET)
    return result


# Generic text model with single text and cleaning process
@app.route('/similarities/generic-text', methods=['POST'])
@cross_origin()
def modelwithtext():
    text = request.form['content']
    clean_text = ped.clean_text(text)
    # Change to json format
    clean_text = {"content": clean_text}
    # Select model that applies
    model = os.path.join(here, 'd2v_model_pickle.pickle')
    result = get_similar(content=clean_text, full_text=text, model_file=model, PATH_TO_DATASET=PATH_TO_DATASET)
    return result

    

#endtime



# Helper function to check if file is csv
def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() == 'csv'


# Update files and models
@app.route('/model-records', methods=["POST"])
def update_model():
    # Check for authorization
    if request.authorization and request.authorization.username == 'username' and request.authorization.password == 'sY8@01lq!.0sg,upq&z9':

        label = request.files.get('label')
        standardized = request.files.get('standardized')
        
        if not label and not standardized:
            return make_response(jsonify({
                'success': False,
                'message': 'Required files not in request'
            }), 400)

        if label:
            if allowed_file(label.filename):
                # Save file to data path
                label.save(os.path.join(PATH_TO_DATASET, 'AI_LABELLING.csv'))
            else:
                return make_response(jsonify({
                'success': False,
                'message': 'Wrong file format'
            }), 400)

        if standardized:
            if allowed_file(standardized.filename):
                # Save file to data path
                standardized.save(os.path.join(PATH_TO_DATASET + 'Standardization.csv'))
            else:
                return make_response(jsonify({
                'success': False,
                'message': 'Wrong file format'
            }), 400)
  
        try:
            # Update files and model
            updatemodels.update(PATH_TO_DATASET)

            return make_response(jsonify({
            'success': True,
            'message': 'File(s) and models updated'
        }), 200)
        except:
            return jsonify({
            'success': False,
            'message': 'An error occured, could not update models'
        })

    return make_response('Could not verify!', 401, {'WWW-Authenticate' : 'Basic realm="Login Required"'})
    


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8080, debug=True)
