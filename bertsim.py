import sentence_transformers as st
import pandas as pd
import numpy as np
from scipy import spatial
import random

st_model = st.SentenceTransformer('distilbert-base-nli-mean-tokens')


def run_model(data, n_id):
    st_vecs_list = st_model.encode(list(data['CONTENT']))
    st_dict = {}
    for index, doc in enumerate(st_vecs_list):
        st_dict[index] = {}
    for idx1, vec1 in enumerate(st_vecs_list):
        for idx2, vec2 in enumerate(st_vecs_list):
            st_dict[idx1][idx2] = 1 - spatial.distance.cosine(vec1, vec2)
    st_matrix = pd.DataFrame(st_dict)
    st_matrix.columns = list(data['idd'])
    st_matrix.index = list(data['idd'])
    st_matrix
    print("first step done......")

    tt = st_matrix[n_id]
    tttt = pd.DataFrame(st_matrix[n_id])
    # tttt
    data2 = []
    for i in tttt[n_id]:
        if i >= 0.75:
            i = "similar"
        elif i < 0.75:
            i = "Not similar"
        data2.append(i)
    indd = tttt.index
    ind = pd.DataFrame(indd, columns=['index'])
    ss = pd.DataFrame(data2, columns=['score'])
    ss.index = list(indd)

    print("second step done......")

    # find similarity and text of the similar case

    # find similarity and text of the similar case
    findsim = ss.loc[lambda ss: ss['score'] == 'similar']
    ind_findsim = findsim.index.to_list()
    for i in ind_findsim:
        ff = i
        # print(ff)
        closesim_cases = data.loc[lambda data: data['idd'] == ff]
        # print(closesim_cases[['SUIT NUMBER', 'CONTENT']])
        resp = closesim_cases[['idd', 'CONTENT']]
        # jsonn = dict(resp)
        jsonn = resp.to_json()
        return jsonn
