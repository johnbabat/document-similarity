# %% Helper functions.
#from bs4 import BeautifulSoup
import nltk
from nltk.corpus import stopwords
import string
import re

nltk.download('stopwords')

stopwords_text = set(stopwords.words('english'))
stopwords_facts = set(stopwords.words('english'))

stopwords_text |= {"the", "appeal", "appellant", "evidence", "respondent", "case", "trial", "learned", "law",\
					"counsel", "appellants", "it", "judgment", "part", "nwlr", "state", "issue",\
					"respondents", "in", "said", "justice", "order", "section", "that",\
					"he", "exhibit", "act", "judge", "defendant", "also", "whether", "plaintiff"}

stopwords_facts |= {'the', 'of', 'to', 'and', 'court', 'in', 'was', 'on', 'a', 'that', 'trial', 'for',\
               		'by', 'plaintiff', 'whether', 'defendant', 'appeal', 'an', 'at', 'as', '1st', 'not',\
               		'Plaintiffs', 'with', 'filed', 'accused', 'appeal', 'considered', 'defendants', 'is',\
               		'his', 'appealed', 'order', 'right', 'judge', '2nd', 'judgment', 'it', 'from', 'he',\
               		'were', 'which', 'its', 'high', 'before', 'learned', 'issues', 'or', 'case', 'against'}



def content_cleaning(data):
	data = data.replace('&nbsp', '')
	data = data.replace('o/o', 'percent')
	data = data.replace('%', 'percent')
	data = data.replace('\s?-\s?', 'to')
	data = data.replace(' writ ', 'write')
	data = data.replace('&amp', 'and')
	data = data.replace('â€‹', '')
	data = data.replace('ã¢â€â‹', '')
	data = re.sub('<br\s*?>', '\n', data)
	data = data.replace('</p>', '\n')
	return data


def final_cleaner(data):
	# Some punctuations are needed to be kept in place for later regex matching
  	punctuations = set(string.punctuation)
  	punctuations |= set(['â', '€', 'ã', '¢'])

  	# Keep the following punctuations
  	punctuations.remove('$')
  	punctuations.remove('#')
  	punctuations.remove('/')
  	punctuations.remove('&')
  	# Added other observed characters that need to be removed
  	remaining_punctuations = set(['$','#','/','&', '\\', 'â', '€', 'ã', '¢'])

  	# remove first set of punctuations
  	temp = [char for char in data if char not in punctuations]
  	data = "".join(temp).strip('')

  	#Other cleaning
  	data = data.replace('\u200b', '')
  	data = data.replace('/', '')
  	data = re.sub(r'(\s|^)(N|\$|\#)\d(\w+)?(\s\d+)?(\s\d+)?(K|k)?(\s|$)', ' money ', data)
  	data = re.sub(r'@', 'at', data)
  	data = re.sub(r'^\w{1,3}\.', '', data)
  	data = re.sub(r'(\s|^)((X){1,3})?(IX|IV|V)?I{0,3}(\s|\.)', '', data, flags=re.IGNORECASE)
  	data = re.sub(r'^\d+', ' ', data)
  	data = re.sub(r'^\w\s', ' ', data)
  	data = re.sub(r'\d+(\s)?pm', ' evening time ', data)
  	data = re.sub(r'\d+(\s)?am', ' morning time ', data)
  	data = re.sub(r'esq', ' esquire ', data, flags=re.IGNORECASE)
  	data = re.sub(r'icpc', ' independent corrupt practices and other related offences commission', data, flags=re.IGNORECASE)
  	data = re.sub(r'\s(df1|p3|p1|d5|df2|p2|j1|p4|j2|b6|az1|az4|b1|p5|p6)\s', ' exhibit label ', data, flags=re.IGNORECASE)
  	data = re.sub(r'\sexh\s', ' exhibit ', data, flags=re.IGNORECASE)
  	data = re.sub(r'\spw(s|d)?\s', ' prosecution witness ', data, flags=re.IGNORECASE)
  	data = re.sub(r'\sdw(s|d)\s', ' defence witness ', data, flags=re.IGNORECASE)
  	data = re.sub(r'\scbn\s', ' central bank of nigeria ', data, flags=re.IGNORECASE)
  	data = re.sub(r'\sacb\s', ' african continental bank ', data, flags=re.IGNORECASE)
  	data = re.sub(r'\sfrn\s', ' federal replublic of nigeria ', data, flags=re.IGNORECASE)
  	data = re.sub(r'\sjca', ' justice court of appeal ', data, flags=re.IGNORECASE)
  	data = re.sub(r'\sfhc\s', ' federal high court ', data, flags=re.IGNORECASE)
  	data = re.sub(r'fcda', ' federal capital development authority ', data, flags=re.IGNORECASE)
  	data = re.sub(r'\sabj\s', ' abuja ', data, flags=re.IGNORECASE)
  	data = re.sub(r'\svol\s', ' volume ', data, flags=re.IGNORECASE)
  	data = re.sub(r'\smisc\s', ' miscellaneous ', data, flags=re.IGNORECASE)
  	data = re.sub(r'\sie\s', ' that is ', data, flags=re.IGNORECASE)
  	data = re.sub(r'cpl\sattabo', 'corporal attabo', data, flags=re.IGNORECASE)
  	data = re.sub(r'jsc', ' justice supreme court ', data, flags=re.IGNORECASE)
  	data = re.sub(r'\ssan\s', ' senior advocate of nigeria ', data, flags=re.IGNORECASE)
  	data = re.sub(r'\scpl\s', ' criminal procedure law ', data, flags=re.IGNORECASE)
  	data = re.sub(r'anor\s', ' another ', data, flags=re.IGNORECASE)
  	data = re.sub(r'\spt\s', ' part ', data, flags=re.IGNORECASE)
  	data = re.sub(r'\spg\s', ' page ', data, flags=re.IGNORECASE)
  	data = re.sub(r'\s(hon|honorable)\s', ' honourable ', data, flags=re.IGNORECASE)
  	data = re.sub(r'cf001', ' inec personal particulars form ', data, flags=re.IGNORECASE)
  	data = re.sub(r'cf002', ' names of candidates of a political party form ', data, flags=re.IGNORECASE)
  	data = re.sub(r'\sacja\s', ' administration of criminal justice act ', data, flags=re.IGNORECASE)
  	data = re.sub(r'\s+', ' ', data)

  	chars = [char.lower() for char in data if char not in remaining_punctuations]
  	return ''.join(chars).strip('')


def clean_text(text):
  text = content_cleaning(text)
  word_list = final_cleaner(text).split()
  clean_text = " ".join([word for word in word_list if word not in stopwords_text])
  return clean_text

def clean_facts(text):
  text = content_cleaning(text)
  word_list = final_cleaner(text).split()
  clean_text = " ".join([word for word in word_list if word not in stopwords_facts])
  return clean_text